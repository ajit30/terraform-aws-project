terraform {
  backend "s3" {
    bucket = "terraform-aws-bucket-gautam"
    key    = "terraform/terraform-aws.tfstate"
    region = "ap-south-1"
    encrypt = true
  }
}
